官网当中也明确提供了自定义FeignClient，以下是在官网基础上对自定义FeignClient的一个简单封装

首先创建FeignClientConfigurer类，这个类相当于build FeignClient的工具类

import feign.*;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.slf4j.Slf4jLogger;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Import;

@Import(FeignClientsConfiguration.class)
public class FeignClientConfigurer {

    private Decoder decoder;
    private Encoder encoder;
    private Client client;
    private Contract contract;

    public FeignClientConfigurer(Decoder decoder, Encoder encoder, Client client, Contract contract) {
        this.decoder = decoder;
        this.encoder = encoder;
        this.client = client;
        this.contract = contract;
    }

    public RequestInterceptor getUserFeignClientInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate requestTemplate) {
                // 添加header
            }
        };
    }

    public <T> T buildAuthorizedUserFeignClient(Class<T> clazz, String serviceName) {
        return getBasicBuilder().requestInterceptor(getUserFeignClientInterceptor())
                //默认是Logger.NoOpLogger
                .logger(new Slf4jLogger(clazz))
                //默认是Logger.Level.NONE（一旦手动创建FeignClient，全局配置的logger就不管用了，需要在这指定）
                .logLevel(Logger.Level.FULL)
                .target(clazz, buildServiceUrl(serviceName));
    }

    private String buildServiceUrl(String serviceName) {
        return "http://" + serviceName;
    }

    protected Feign.Builder getBasicBuilder() {
        return Feign.builder().client(client).encoder(encoder).decoder(decoder).contract(contract);
    }
}
使用工具类的方法创建多个FeignClient配置

import com.gzl.cn.service.FeignTest1Service;
import feign.Client;
import feign.Contract;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignClientConfiguration extends FeignClientConfigurer {
    public FeignClientConfiguration(Decoder decoder, Encoder encoder, Client client, Contract contract) {
        super(decoder, encoder, client, contract);
    }

    @Bean
    public FeignTest1Service feignTest1Service() {
        return super.buildAuthorizedUserFeignClient(FeignTest1Service.class, "CLOUD-PAYMENT-SERVICE");
    }

    // 假如多个FeignClient在这里定义即可
}
其中，super.buildAuthorizedUserFeignClient()方法中，第一个参数为调用别的服务的接口类，第二个参数为被调用服务在注册中心的service-id。

public interface FeignTest1Service {

    @GetMapping(value = "/payment/get/{id}")
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);
}
使用的时候正常注入使用即可

@Resource
private FeignTest1Service feignTest1Service;